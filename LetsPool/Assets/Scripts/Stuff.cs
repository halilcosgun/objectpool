﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Stuff : MonoBehaviour
{
    private MeshRenderer[] _meshRenderers;
    public Rigidbody Body { get; private set; }


   

    private void Awake()
    {
        Body = GetComponent<Rigidbody>();
        _meshRenderers = GetComponentsInChildren<MeshRenderer>();

    }

    public void SetMaterial(Material m)
    {
        for (int i = 0; i < _meshRenderers.Length; i++)
        {
            _meshRenderers[i].material = m;
        }
    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Kill Zone")
        {
            Destroy(gameObject);
        }
    }
}
