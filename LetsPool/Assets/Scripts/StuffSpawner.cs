﻿using UnityEngine;
using System.Collections;

public class StuffSpawner : MonoBehaviour {

    public Stuff[] StuffPrefabs;
    public FloatRange TimeBetweenSpawns;
    public FloatRange Scale;
    public FloatRange RandomVelocity;
    public FloatRange AngularVelocity;
    public float Velocity;

    public Material StuffMaterial;

    private float _timeSinceLastSpawn;
    private Transform _transform;
    private float _currentSpawnDelay;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        _timeSinceLastSpawn += Time.deltaTime;
        if (_timeSinceLastSpawn >= _currentSpawnDelay)
        {
            _timeSinceLastSpawn -= _currentSpawnDelay;
            _currentSpawnDelay = TimeBetweenSpawns.RandomRange;
            SpawnStuff();
        }
    }

    private void SpawnStuff()
    {
        var prefab = StuffPrefabs[Random.Range(0, StuffPrefabs.Length)];
        var spawn = Instantiate(prefab);
        spawn.transform.localPosition = _transform.position;
        spawn.transform.localScale = Vector3.one*Scale.RandomRange;
        spawn.transform.localRotation = Random.rotation;

        spawn.Body.velocity = _transform.up * Velocity + Random.onUnitSphere * RandomVelocity.RandomRange;
        spawn.Body.angularVelocity = Random.onUnitSphere * AngularVelocity.RandomRange;

        spawn.SetMaterial(StuffMaterial);
    }
}
