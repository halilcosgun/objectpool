﻿using UnityEngine;
using System.Collections;

public class StaffSpawnerRing : MonoBehaviour {


    public int NumberOfSpawners;
    public float Radius;
    public float TiltAngle;
    public StuffSpawner SpawnerPrefab;
    public Material[] StuffMaterials;




    private void Awake()
    {
        for (var i = 0; i < NumberOfSpawners; i++)
        {
            CreateSpawner(i);
        }
    }

    private void CreateSpawner(int index)
    {
        var rotater = new GameObject("Rotater").transform;
        rotater.SetParent(transform, false);
        rotater.localRotation =
            Quaternion.Euler(0f, index * 360f / NumberOfSpawners, 0f);

        var spawner = Instantiate(SpawnerPrefab);
        spawner.transform.SetParent(rotater, false);
        spawner.transform.localPosition = new Vector3(0f, 0f, Radius);
        spawner.transform.localRotation = Quaternion.Euler(TiltAngle, 0f, 0f);

        spawner.StuffMaterial = StuffMaterials[index % StuffMaterials.Length];
    }
}
