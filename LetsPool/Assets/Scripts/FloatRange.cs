﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct FloatRange
{

    public float Min;
    public float Max;

    public float RandomRange
    {
        get { return Random.Range(Min, Max); }
    }
}
